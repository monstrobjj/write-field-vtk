program main

  implicit none
  
  ! numerical constants
  real(kind=4), parameter :: pi = acos(-1.0)
  
  ! auxiliares de loop
  integer :: i, j, k
  character(len=1024) :: buffer
  
  ! numero dos pontos de grade nas direções x, y e z
  integer, parameter :: nx = 16
  integer, parameter :: ny = 16
  integer, parameter :: nz = 16
  
  ! tamanho da grade (dimensões da galáxia ~ 40 kpc)   
  real(kind=4), parameter :: lx = (40.0*3.0e21) ! cm
  real(kind=4), parameter :: ly = (40.0*3.0e21) ! cm
  real(kind=4), parameter :: lz = (40.0*3.0e21) ! cm
  
  ! parametros da onda
  real(kind=4), parameter :: ay = 0.5    ! Gauss
  real(kind=4), parameter :: phil = 1.5  ! rad
  
  ! distancia entre os pontos da grade
  real(kind=4) :: dx
  real(kind=4) :: dy
  real(kind=4) :: dz
  
  ! tempo do campo
  real(kind=4) :: time = 0. ! s
  
  ! campo magnético b = (bx, by, bz)
  real(kind=4), dimension(nx,ny,nz) :: bx
  real(kind=4), dimension(nx,ny,nz) :: by
  real(kind=4), dimension(nx,ny,nz) :: bz
  
  ! coordenadas dos pontos de grade
  real(kind=4), dimension(nx) :: x 
  real(kind=4), dimension(ny) :: y
  real(kind=4), dimension(nz) :: z
  
  ! matriz temporario
  real(kind=4), dimension(:,:,:), allocatable :: tmp_float
  
    
  dx = lx / real(nx - 1)
  dy = ly / real(ny - 1)
  dz = lz / real(nz - 1)
  
  !write (unit=*, fmt=*) "dx = ", dx, ' cm' 
  
  ! construção da grade física
  do i = 1, nx
    x(i) = - lx/2.0 + real(i - 1)*dx
    !write (unit=*, fmt=*) "x(", i, ") =  ", x(i)   
  enddo
  
  do j = 1, ny
    y(j) = - ly/2.0 + real(j - 1)*dy
  enddo
  
  do k = 1, nz
    z(k) = - lz/2.0 + real(k - 1)*dz
  enddo
  
  
  do k = 1, nz
    do j = 1, ny
      do i = 1, nx
  	bx(i,j,k) = 1.0 				 ! Gauss
  	by(i,j,k) = ay*cos(3.0*x(i)*2.0*pi/lx + phil)	 ! Gauss
  	bz(i,j,k) = 0.0 				 ! Gauss
      enddo
    enddo
  enddo
   
  
  ! escrevendo arquivo VTK
  open(unit=10, file='galaxia.vtk', form='unformatted', access='stream', status='replace', action='write', convert='big_endian')
  
  allocate (tmp_float(nx, ny, nz)) ! pega uma matrix e aloca um espaço na memória

  write(buffer, '(a)') '# vtk DataFile Version 2.0\n'
  write(10) trim(buffer)
  
  write(buffer, '(a)') 'write-field-vtk vtk output\n'
  write(10) trim(buffer)

  write(buffer, '(a)') 'BINARY\n'
  write(10) trim(buffer)

  write(buffer, '(a)') 'DATASET STRUCTURED_POINTS\n'
  write(10) trim(buffer)

  write(buffer, '(a, 3(1x, i0), a)') 'DIMENSIONS', nx, ny, nz, '\n' ! 1x = espaço em branco, i0 = somente os caracteres que preciso
  write(10) trim(buffer)

  write(buffer, '(a, 3(es24.16, 1x), a)') 'ORIGIN', x(1), y(1), z(1), '\n'
  write(10) trim(buffer)

  write(buffer, '(a, 3(es24.16, 1x), a)') 'SPACING', dx, dy, dz, '\n'
  write(10) trim(buffer)

  write(buffer, '(a, (1x, i0), a)') 'POINT_DATA', nx*ny*nz, '\n'
  write(10) trim(buffer)

  write(buffer, '(a)') 'SCALARS bx float\n'
  write(10) trim(buffer)

  write(buffer, '(a)') 'LOOKUP_TABLE default\n'
  write(10) trim(buffer)

  tmp_float = real(bx, 4)
  write(10) tmp_float

  write(buffer, '(a)') 'FIELD FieldData 2\n' ! quantidade de campos que temos
  write(10) trim(buffer)

  write(buffer, '(a, (1x, i0), a)') 'by 1', nx*ny*nz, ' float \n'
  write(10) trim(buffer)

  tmp_float = real(by, 4)
  write(10) tmp_float

  write(buffer, '(a, (1x, i0), a)') 'bz 1', nx*ny*nz, ' float \n'
  write(10) trim(buffer)

  tmp_float = real(bz, 4)
  write(10) tmp_float

  deallocate (tmp_float)
       
  close(unit=10)
  
  !stop "Execucao interrompida"
  write (unit=*, fmt=*) "Execucao finalizada." 
  
end program main
