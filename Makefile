FC = gfortran
FFLAGS = -fbackslash

.PHONY: clean

write.exe: main.f08
	$(FC) $(FFLAGS) -o write.exe main.f08

clean:
	rm *.x *.vtk
